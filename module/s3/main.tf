resource "aws_s3_bucket" "my_bucket" {
 bucket = var.bucket_name
 tags = {
  Name = "${var.tag_name}-bucket"
  Env = "${var.tag_name}"
 }
}

#resource "aws_s3_bucket_acl" "example" {
 # bucket = aws_s3_bucket.my_bucket.id
 # acl    = var.acl
#}

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.my_bucket.id
  key    = var.object_key
  source = var.object_source
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket = aws_s3_bucket.my_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


