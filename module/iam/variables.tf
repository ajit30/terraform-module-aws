variable "role_name" {
 type = string
}
variable "assume_role_policy" {
 type = any
}

variable "policy_name" {
 type = string
}

variable "path" {
 type = any
}

variable "iam_policy_description" {
 type = string
}

variable "policy" {
 type = any
}

variable "instance_profile_name" {
 type = string
}
