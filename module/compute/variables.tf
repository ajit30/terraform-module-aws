variable "ami" {
  type = string
}
variable "instance_type" {
 type = string
}

variable "user_data" {
 type = any
}

variable "key_name" {
 type = string
}

variable "tag_name" {
 type = string
}

variable "vpc_security_group_ids" {
 type = string
}

