module "compute" {
  source                 = "./module/compute"
  ami                    = data.aws_ami.my-ami.id
  instance_type          = "t3.micro"
  tag_name               = "web-server"
  vpc_security_group_ids = "launch-wizard-3"
  user_data              = file("./usedata.tpl")
  key_name               = data.aws_key_pair.my-key-pair.key_name
  #iam_instance_profile   = output.s3_profile

}

data "aws_ami" "my-ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["al2023*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_key_pair" "my-key-pair" {
  key_name           = "terraform"
  include_public_key = true

}

module "iam" {
  source                 = "./module/iam"
  role_name              = "s3-list-bucket"
  policy_name            = "s3-list-bucket"
  instance_profile_name  = "s3-list-bucket"
  path                   = "/"
  iam_policy_description = "s3 policy for ec2 to list role"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:Get*",
          "s3:List*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

module "s3" {
  source        = "./module/s3"
  bucket_name   = "terraform-module-aws-1"
  #acl           = "public-read"
  object_key    = "ajit"
  object_source = "/dev/null"
  tag_name      = "terraform-module-aws"
}

module "security" {
  source = "./module/security"

}

